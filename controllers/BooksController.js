const TrxBooks = require("../models/data");

exports.index = async (req, res, next) => {
    try{
        const data = TrxBooks
        return res.status(200).json({message: 'succsess', data}) 
    }catch(err){
        next(err)
    }
}

exports.show = async (req, res, next) => {
    try{
        const {id} = req.params;
        const data = TrxBooks.find(item => item.id === parseInt(id))
        
        return res.status(200).json({message: 'succsess', data}) 
    }catch(err){
        next(err)
    }
}

exports.showJenis = async (req, res, next) => {
    try{
        const {jenis} = req.params;
        const data = TrxBooks.filter((item) => item.type === jenis)
        
        return res.status(200).json({message: 'succsess', data}) 
    }catch(err){
        next(err)
    }
}

exports.store = async (req, res, next) => {
    try{
        const {
            name,
            type
        } = req.body

        const countID = TrxBooks.slice(-1)[0].id+1;
        const data = {
            id:countID,
            name,
            type,
        }
        const store = [...TrxBooks, data];
        
        return res.status(200).json({message: 'succsess', data:store}) 
    }catch(err){
        next(err)
    }
}

exports.destroy = async (req, res, next) => {
    try{
        const {id} = req.params;
        const updatedTrxBooks = TrxBooks.filter(item => item.id !== id);
        
        return res.status(200).json({message: 'succsess', data: updatedTrxBooks}) 
    }catch(err){
        next(err)
    }
}

exports.update = async (req, res, next) => {
    try{
        const {id} = req.params;
        const {name} = req.body;

        const data = TrxBooks.map(element => {
            if (element.id === parseInt(id)) {
                return { ...element, name: name };
            }
            return element;
        });
        
     
        return res.status(200).json({message: 'succsess', data}) 
    }catch(err){
        next(err)
    }
}

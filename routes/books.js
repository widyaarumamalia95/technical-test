const express = require('express');
const router = express.Router();

const BooksController = require('../controllers/BooksController');

router
  .get('/', BooksController.index);

router
  .get('/:id', BooksController.show);

router
  .get('/jenis/:jenis', BooksController.showJenis);

router
  .delete('/:id', BooksController.destroy);

router
  .put('/:id', BooksController.update);




module.exports = router;